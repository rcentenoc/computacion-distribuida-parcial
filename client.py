# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 00:13:11 2021

@author: Fab
"""
#---------------------------LIBRERIAS
import socket
#Librerias adicionales
import matplotlib.pyplot as plt
import PySimpleGUI as sg
import pandas as pd
#-------------------------------
sg.theme('DarkBlue10')# tema de la consola
print("INICIANDO CLIENTE") # entrada del cliente
ipServidor = "localhost" # nombre del Ipservidor
puertoServidor = 9798 # puerto por el cual se comunicara con el servidor
nombre_equipo = socket.gethostname() #nos entrega el host del dispositivo
cliente = socket.socket(socket.AF_INET, socket.SOCK_STREAM) #establecemos la conexiòn tcp IP
cliente.connect((ipServidor , puertoServidor))# identificará la ip del servidor junto al puerto
print("Conectado al servidor:", (ipServidor, puertoServidor))# Nos indicará que ya nos conectamos al servidor

#-------------------- layout es la consola de ingreso de datos en python por PysimpleGUI
layout = [[sg.Text('Ingrese una función')],
          [sg.Input()],
          [sg.Text('Ingrese el número de trapecios que utilizará')],
          [sg.Input()],
          [sg.Text('Ingrese el punto inicial -> A')],
          [sg.Input()],
          [sg.Text('Ingrese el punto final -> B')],
          [sg.Input()],
                    [sg.OK()] ]

#------------------- PROCESANDO LOS DATOS
event, values = sg.Window('Presione ok para continuar', layout).Read() # leyendo los datos ingresados
sg.Popup(event, values[0]) # Guardando los datos ingresados en el array data antes de enviarlos

#------- la función
funcion = str(values[0]) # guardamos en al variable función el priemr dato ingresado en la consola guardado como values[0]
cliente.send(funcion.encode('utf-8'))# enviamos la función al servidor

#------- nro de trapecios
escucha0 = cliente.recv(1024) # para no perder datos pondremos a escuchar la conexión y guardaremos lo que veng del servidor en la variable escucha0
num_trap = str(values[1]) # guardamos el numero de trapecios en la variable num_trap
cliente.send(num_trap.encode('utf-8'))# enviamos la variable al servidor por medio de la conexión cliente códificada en utf 8

#------- punto inicial A
escucha1 = cliente.recv(1024) # volvemos a poner la conexión en modo escucha
punto_min = str(values[2]) #guardamos el punto de inicio A en la variable punto_min
cliente.send(punto_min.encode('utf-8')) # enviamos la variable al servidor por medio de la conexión cliente códificada en utf 8

#------- punto final B
escucha2 = cliente.recv(1024) # volvemos a poner la conexión en modo escucha
punto_max = str(values[3]) # guardamos el punto final en al variable punto_max
cliente.send(punto_max.encode('utf-8')) # enviamos la variable al servidor por medio de la conexión cliente códificada en utf 8

#----------RECEPCIÓN DE DATOS DEL SERVIDOR
suma = cliente.recv(1024)# se pone en escuha la conexión para recepcionar el resultado sumado del area de los trapecios
tiempo_ejecucion = cliente.recv(1024)# se pone en escuha la conexión para recepcionar el tiempo que demoro el intercambio de datos
print("El tiempo de demora evaluado es : ",tiempo_ejecucion) #se imprime el segundo dato recepcionado
print(" ")
print ("El area trapezoidal total hallada es: ",suma) #Se imprime el primer dato recepcionado, el de la suma de trapecios

#---------- Mandamos la confirmación para que el servidor dibuje el gráfico
consulta = "recepcionado" # ponemos la variable consulta como recepcionada despues de imprimir los datos que llegaron
cliente.send(consulta.encode('utf-8')) # enviamos el valor de la variable al servidor codificado en utf 8 para que el servidor dibuje la gráfica correspondiente

#---------- RECEPCIÓN DE LOS DATOS PARA EMPEZAR A DIBUJAR EL GRÁFICO DE PARTE DEL CLEINTE
xk = cliente.recv(65536).decode('utf-8') # se pone en escucha, con recepción de 65536 caracteres de recepción
fk = cliente.recv(65536).decode('utf-8') # se pone en escucha, con recepción de 65536 caracteres de recepción
xi = cliente.recv(65536).decode('utf-8') # se pone en escucha, con recepción de 65536 caracteres de recepción
fi = cliente.recv(65536).decode('utf-8') # se pone en escucha, con recepción de 65536 caracteres de recepción
lista_area_trapecios = cliente.recv(1024).decode('utf-8') # se pone en escucha, con recepción de 65536 caracteres de recepción
limite_grafico = cliente.recv(1024).decode('utf-8')# se pone en escucha, con recepción de 65536 caracteres de recepción

#--------- MEDIANTE PANDAS IMPRIMIREMOS LOS RESULTADOS ENVIADOS POR EL SERVIDOR
lista_definitiva =list(range(1,int(values[1])+2)) # la variable lista_definitiva ordenara el numero de trapecios utilizados
df = pd.DataFrame(list(zip(lista_definitiva,eval(lista_area_trapecios))),columns=['trapecios','area']) # creamos un dataframe para poder leer de forma estadistica los datos resueltos
print(df)#imprimimos el dataframe

#--------- GRAFICO
plt.plot(eval(xk),eval(fk), color='c',label ='f(x)')
plt.plot(eval(xi),eval(fi), marker='o',color='darkblue', label ='areas_trapecios')

plt.xlabel('x')
plt.ylabel('f(x)')
plt.title('Regla de Trapecios')

#--------- DIBUJO DE LOS TRAPECIOS
plt.fill_between(eval(xi),0,eval(fi), color='lime')
for i in range(0,int(limite_grafico),1):
    plt.axvline(eval(xi)[i], color='r', linestyle='dashed')
    
#-------------




#-----------
plt.show()
cliente.close()






