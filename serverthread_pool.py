# -*- coding: utf-8 -*-
"""
Created on Wed Oct 13 06:53:40 2021

@author: Fab
"""
import socket
#-----------------------------
#librerias adicionales para el calculo del area del trapecio
import time
import logging
import numpy as np
import matplotlib.pyplot as plt
from concurrent.futures import ThreadPoolExecutor #Una clase abstracta que proporciona métodos para ejecutar llamadas de forma asincrónica. 
"""
El concurrent.futures se creo para administrar y crear subprocesos de manera eficiente
Sintaxis: concurrent.futures.ThreadPoolExecutor(max_workers=None, thread_name_prefix=", initializer=None, initargs=())
Parámetros:
► max_workers:Es un número de hilos también conocido como tamaño de grupo. A partir de 3,8 el valor predeterminado es min(32, os.cpu_count() + 4). De estos 5 subprocesos se conservan para la tarea enlazada de E/S.
► thread_name_prefix : thread_name_prefix se agregó desde python 3.6 en adelante para dar nombres a los subprocesos para facilitar la depuración.
► inicializador: el inicializador toma un invocable que se invoca al inicio de cada subproceso de trabajo.
► initargs: Es una tupla de argumentos pasados al inicializador.
"""
#-----------------------------
logging.basicConfig(level=logging.DEBUG, format='%(threadName)s %(message)s')# A medida que se van realizando la ejecución de los hilos, este los capturara y los mostrará
#format="%(asctime)s %(levelname)s %(threadName)s %(name)s %(message)s" ► tambien puede ser este format
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)#comonicaciòn tcp IP en la variable s que representa al servidor
s.bind(("", 9798))  #dejamos el host vacio y establecemos el puerto 
s.listen(5) #cantidad de cliente sque estarian en espera
print("xXx--- Servidor iniciado ---xXx")
# Se recibe al cliente
cliente, addr=s.accept() #aceptamos la conexiòn 
print ("El cliente "+str(addr)+" se ha conectado")

#-------------funciòn
#variables a usar para el calculo de la funciòn
global fi, xi,dx, vect, var
form=0

#Funcion para calcular area de cada trapecio
def area_trapecio():
    global form
    form = abs(round((dx*(fi[var]+fi[var+1])/2),2))

#----------------------------
while True:
    
    # Se espera a un cliente
    #cliente_sock, cliente_dat = s.accept()
    
    minuto_inicio = time.time() # se inicia el conteo solicitado en los paramtros del parcial
    respuesta ="recepcionado"
    #------------------ recibiendo los datos del cliente
    consulta = (cliente.recv(1024)).decode('utf-8') # recibiendo los datos con 1024 caracteres decodificados en utf-8
    fx = lambda x: eval(consulta) # variable que guarda la función ingresada por el cliente       
    cliente.send(respuesta.encode())
    num_trap = (cliente.recv(1024)).decode('utf-8')
    vect = int(num_trap) # variable que recibira el número de trapecios
    cliente.send(respuesta.encode())
    punto_min = (cliente.recv(1024)).decode('utf-8')
    a = int(punto_min) # variable que recibira el punto incial de calculo
    cliente.send(respuesta.encode())
    punto_max = (cliente.recv(1024)).decode('utf-8')
    b = int(punto_max) # variable que recibira el punto final de calculo

    # -------------------
    limite_grafico = vect + 1
    xi = np.linspace(a,b,limite_grafico) # Linspace devuelve números espaciados uniformemente durante un intervalo especificado en abse al limite grafico
    fi = fx(xi) # Se recrea el array linspace para ingresarlo en la función
    
    #-------------------- inicializando varaibles y arrays que utilizará la función
    suma = 0 # variable que guardara los resultados del area del trapecio en base a i
    lista_area_trapecios=[] # Ira guardando los resultados del area del trapecio en base a i
    threads_max=4
    #-------------------- HILOS
    for i in range(0,vect,1):
        var = i # el valor de x va subiendo de 1 en 1
        dx = xi[i+1]-xi[i]# ordena los arrays creados por linspace
        #------------------POOL
        exe=ThreadPoolExecutor(threads_max)# ThreadPoolExecutor nos ayudara al multiprocesamiento del pool de hilos 
        f = exe.submit(area_trapecio) # Submit ejecuta un método invocable y devuelve un objeto Future que representa el estado de ejecución del método.
    #-----------------------
        lista_area_trapecios.append(form) # agrega el resultado del area del trapecio i
        suma = suma + form # suma de todas las areas calculadas, agregando por area i    
    #---------------------
        """
        Existe otros dos métodos además de submit para ejecutar el pool de hilos
        
        submit(fn, *args, **kwargs)
        
        map(fn, *iterables, timeout = None, chunksize = 1) 
                Mapea el método e itera juntos inmediatamente y planteará una excepción concurrente. Futuros. TimeoutError si no lo hace dentro del límite de tiempo de espera.
                Si las iteraciones son muy grandes, entonces tener un tamaño de trozo mayor que 1 puede mejorar el rendimiento cuando se usa ProcessPoolExecutor pero con ThreadPoolExecutor no tiene tal ventaja, es decir, se puede dejar a su valor predeterminado.
        
        shutdown(wait = True, *, cancel_futures = False)
                Le indica al ejecutor que libere todos los recursos cuando los futuros terminen de ejecutarse.
                Debe llamarse antes del método executor.submit() y executor.map() de lo contrario arrojaría RuntimeError.
                wait=True hace que el método no regrese hasta que se realice la ejecución de todos los subprocesos y se libere recursos.
                cancel_futures=True, el ejecutor cancelará todos los subprocesos futuros que aún no se han comenzado.

    #-------- OTRA FORMA DE TRABJAR CON SUBMIT
        with ThreadPoolExecutor(threads_max) as exe:
            exe.submit(area_trapecio)
            var = exe.map(area_trapecio)
    for r in var:
        print(r)
        
        """    
        
    minuto_fin = time.time()#termina el conteo de tiempo solicitado en el parcial
    tiempo_ejecucion = minuto_fin - minuto_inicio # calculamos el tiempo de ejecución
    print("El tiempo de demora evaluado en el pool de hilos es : ",tiempo_ejecucion)
      
    #--------------- enviando datos al cliente      
    cliente.send(str(suma).encode('utf-8')) # enviando los resultados de todos los trapecios
    cliente.send(str(round(tiempo_ejecucion,5)).encode('utf-8')) # enviando el tiempo de ejecución
    
    #--------------- última consulta
    consulta2 = (cliente.recv(1024)).decode('utf-8') #consultando respuesta del cliente
    
#--------------------------------------------------- CREANDO LA GRÁFICA
    if consulta2 == "recepcionado": # si el cliente responde que recepciono los datos enviados se procede a dibujar la gráfica

        limite_grafico_linea = vect*10 + 1 # definimos el tamaño y suavidad de las lineas del gráfico
        xk = np.linspace(a,b,limite_grafico_linea) # creamos un xk para que tome el valor de xi
        fk = fx(xk) # creamos un fk para que tome el valor de fi pero esta vez en base a xk
        
        #---------- Creamos 5 strings para que pasen lso valores al cliente de las listas creadas antes y separadas por comas
        str1=','.join(str(round(x,2)) for x in xk)
        str2=','.join(str(round(y,2)) for y in fk)
        str3=','.join(str(round(z,2)) for z in xi)
        str4=','.join(str(round(q,2)) for q in fi)
        str5=','.join(str(w) for w in lista_area_trapecios)
        
        #--------- Enviamos las listas por bytes al cliente, por solicitud del parcial enviamos todos los datos obtenidos
        cliente.send(str1.encode('utf-8'))
        cliente.send(str2.encode('utf-8'))
        cliente.send(str3.encode('utf-8'))
        cliente.send(str4.encode('utf-8'))
        cliente.send(str5.encode('utf-8'))
        cliente.send(str(limite_grafico).encode('utf-8'))


        #--------- GRÁFICO
        plt.plot(xk,fk, color='c',label ='f(x)') #definimosel label que aparecera para el servidor
        plt.plot(xi,fi, marker='o',color='darkblue', label ='areas_trapecios')

        plt.xlabel('x')
        plt.ylabel('f(x)')
        plt.title('Regla de Trapecios')
        plt.legend()

        #--------- DIBUJO DE LOS TRAPECIOS
        plt.fill_between(xi,0,fi, color='lime')
        for i in range(0,limite_grafico,1):
            plt.axvline(xi[i], color='r', linestyle='dashed')

        #plt.show()
        break


